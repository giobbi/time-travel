### Architecture Concepts

The project uses some open-source frameworks like [Spring Boot](https://spring.io/projects/spring-boot), [Spring Data](https://spring.io/projects/spring-data), [Gradle](https://gradle.org/), [Lombok](https://projectlombok.org), [Docker](https://www.docker.com/) and [Swagger](https://swagger.io/).

It has a Domain Driven Design approach, so as previously stated, its context defines the responsability to manage the time travels states. Also, the application layer is separated from the service and persistence layer which allows the project to be more maintainable as the modules are split apart.

### Prerequisites

This project uses Lombok to generate some boilerplate code thus make the code cleaner. Its [plugin](https://projectlombok.org/setup/overview) is required to compile the project in your IDE.

Be sure to have both [docker](https://docs.docker.com/install/) and [docker-compose](https://docs.docker.com/compose/install/) installed.

### Getting Started

Once cloned, grant execution permission to the setup script by running `chmod +x ./setup.sh` and then run the script `./setup.sh`. It will create the MySQL container and the database structure.

Start the application by running `./gradlew bootRun` on your terminal.
The application will run on port `8080` and the swagger documentation page can be accessed in http://localhost:8080/swagger-ui.html.

## Registering a new time travel

Request
```
POST /time-travels { "pgi": "P12345", "place": "london", "date": "2018-07-25T12:15:10.000Z" }
```
> Disclaimer: the date field must be formatted as ISO 8601

Possible responses

- `201 CREATED`
  - `{ "id": 1, "pgi": "P12345", "place": "london", "date": "2018-07-25T12:15:10" }`

- `409 CONFLICT`
  - `{ "errors": [ "user already time traveled to that moment and place" ] }`

- `400 BAD REQUEST`
  - `{ "errors": [ "pgi must be valid" ] }`
  - `{ "errors": [ "place must have less than {max} characters long" ] }`

## Searching time travels

Request
```
GET /time-travels/search?pgi={pgi}&place={place}&date={date}
```
> Disclaimer: the date field must be formatted as ISO 8601

Possible responses (pgi=P12345)

- `200 OK`
  - `[ { "id": 1, "pgi": "P12345", "place": "london", "date": "2018-07-25T12:15:10" }, { "id": 2, "pgi": "P12345", "place": "manchester", "date": "2031-07-23T18:00:00" } ]`

- `404 NOT FOUND`

- `400 BAD REQUEST`
  - `{ "errors": [ "search must have at least one filter" ] }`

### Tests

To run tests just run `./gradlew test`. It will run unit and integration tests.

### Contributing

Use [gitflow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow) and create [cool PRs](https://www.atlassian.com/blog/git/written-unwritten-guide-pull-requests).

### TODO

The next steps.

- [ ] Complete configuration of dockerized app
- [ ] Database migration (Flyway)
- [ ] Implement CI (build, tests, package, db migration, deploy, healthchecks, notifications)

### Contact
- [me](https://github.com/guilhermeiobbi) (guilherme.iobbi@gmail.com).

### License

Licensed under the [MIT license](LICENSE).