#!/bin/bash

#############
# Constants #
#############
CONTAINER_NAME=time_travel.mysql
SCRIPT_PATH=/tmp/time_travel_setup.sh

###########
# Logging #
###########
function log {
    printf "\n`date "+%H:%M:%S"` [SETUP] [$tag] $1"
}

#########
# START #
#########
tag="START"
log "Starting script..."

##########
# Docker #
##########
tag="Docker"
log "Running docker-compose...\n\n"
docker-compose up -d

#########
# MySQL #
#########
tag="MySQL"
function createTable {
    log "Creating database structure...\n\n"
    docker exec -it $CONTAINER_NAME /bin/bash $SCRIPT_PATH
}
count=0
until createTable; do
    log "Error: waiting database startup..."
    sleep 10
    let "count++"
    log "Trying again...(${count})"
done
log "Database script executed succesfully!"

#######
# END #
#######
tag="DONE"
log "Script executed succesfully."