FROM openjdk:8-jdk-alpine

LABEL maintainer="guilherme.iobbi@gmail.com"

VOLUME /tmp

EXPOSE 8080

ARG JAR_FILE=time-travel-api/build/libs/time-travel-api-1.0.0.jar

ADD ${JAR_FILE} time-travel.jar

ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/time-travel.jar"]
