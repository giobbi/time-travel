package com.gamesys.timetravel.api;

import com.gamesys.timetravel.api.dto.ResponseError;
import com.gamesys.timetravel.api.resource.request.TimeTravelRequest;
import com.gamesys.timetravel.api.resource.response.TimeTravelResponse;
import lombok.val;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.util.UriComponentsBuilder;

import java.time.LocalDateTime;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class TimeTravelResourceTest {

    private static final String TIME_TRAVEL_URI = "/time-travels";
    private static final String VALID_PGI = "A12345";
    private static final String INVALID_PGI = "345";
    private static final String VALID_PLACE = "london";

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Test
    public void when_registerTimeTravel_then_success() {
        // arrange
        val request = createValidRequest(VALID_PGI);

        // act
        val response = testRestTemplate.postForEntity(TIME_TRAVEL_URI, request, Void.class);

        // assert
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertNull(response.getBody());
    }

    @Test
    public void when_registerTimeTravel_then_conflict() {
        // arrange
        val customPgi = "AB1234";
        val request = createValidRequest(customPgi);

        // act
        testRestTemplate.postForEntity(TIME_TRAVEL_URI, request, Void.class);
        val response = testRestTemplate.postForEntity(TIME_TRAVEL_URI, request, ResponseError.class);

        // assert
        assertEquals(HttpStatus.CONFLICT, response.getStatusCode());
        assertEquals(1, response.getBody().getErrors().size());
        assertEquals("user already time traveled to that moment and place", response.getBody().getErrors().get(0).toString());
    }

    @Test
    public void when_registerTimeTravel_then_badRequest() {
        // arrange
        val request = createValidRequest(INVALID_PGI);

        // act
        val response = testRestTemplate.postForEntity(TIME_TRAVEL_URI, request, ResponseError.class);

        // assert
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertEquals(1, response.getBody().getErrors().size());
        assertTrue(response.getBody().getErrors().get(0).toString().contains("pgi"));
    }

    @Test
    public void when_registerTimeTravel_then_badRequest_2() {
        // arrange
        val request = new TimeTravelRequest();

        // act
        val response = testRestTemplate.postForEntity(TIME_TRAVEL_URI, request, ResponseError.class);

        // assert
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertEquals(3, response.getBody().getErrors().size());
    }

    @Test
    public void when_searchTimeTravel_then_success() {
        // arrange
        val pgi = "T12345";
        val place = "london";
        val request = TimeTravelRequest.builder()
            .pgi(pgi)
            .place(place)
            .date(LocalDateTime.now())
            .build();
        testRestTemplate.postForEntity(TIME_TRAVEL_URI, request, Void.class);
        val builder = UriComponentsBuilder.fromUriString(TIME_TRAVEL_URI)
            .path("/search")
            .queryParam("pgi", pgi)
            .queryParam("place", place);

        // act
        val response = testRestTemplate.getForEntity(builder.toUriString(), TimeTravelResponse[].class);

        val list = Arrays.asList(response.getBody());
        // assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(1, list.size());
        assertEquals(pgi, list.get(0).getPgi());
        assertEquals(place, list.get(0).getPlace());
    }

    @Test
    public void when_searchTimeTravel_then_notFound() {
        // arrange
        val pgi = "T12345";
        val place = "london";
        val builder = UriComponentsBuilder.fromUriString(TIME_TRAVEL_URI)
            .path("/search")
            .queryParam("pgi", pgi)
            .queryParam("place", place);

        // act
        val response = testRestTemplate.getForEntity(builder.toUriString(), Void.class);

        // assert
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertNull(response.getBody());
    }

    @Test
    public void when_searchTimeTravel_then_badRequest() {
        // arrange
        val builder = UriComponentsBuilder.fromUriString(TIME_TRAVEL_URI)
            .path("/search");

        // act
        val response = testRestTemplate.getForEntity(builder.toUriString(), ResponseError.class);

        // assert
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertEquals(1, response.getBody().getErrors().size());
        assertEquals("search must have at least one filter", response.getBody().getErrors().get(0).toString());
    }

    @Test
    public void when_searchTimeTravel_then_PGIbadRequest() {
        // arrange
        val pgi = "000000";
        val builder = UriComponentsBuilder.fromUriString(TIME_TRAVEL_URI)
            .path("/search")
            .queryParam("pgi", pgi);

        // act
        val response = testRestTemplate.getForEntity(builder.toUriString(), ResponseError.class);

        // assert
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertEquals(1, response.getBody().getErrors().size());
        assertTrue(response.getBody().getErrors().get(0).toString().contains("pgi"));
    }

    private static TimeTravelRequest createValidRequest(final String pgi) {
        return TimeTravelRequest.builder()
            .pgi(pgi)
            .place(VALID_PLACE)
            .date(LocalDateTime.now())
            .build();
    }
}
