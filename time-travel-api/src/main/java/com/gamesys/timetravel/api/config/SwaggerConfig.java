package com.gamesys.timetravel.api.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.ResponseEntity;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.service.Contact;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.time.LocalDateTime;

import static java.util.Arrays.asList;
import static org.apache.logging.log4j.util.Strings.EMPTY;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static springfox.documentation.builders.PathSelectors.any;
import static springfox.documentation.builders.RequestHandlerSelectors.basePackage;
import static springfox.documentation.spi.DocumentationType.SWAGGER_2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket api () {
        return new Docket(SWAGGER_2)
                .select()
                .apis(basePackage("com.gamesys.timetravel.api.resource"))
                .paths(any())
                .build()
                .directModelSubstitute(LocalDateTime.class, String.class)
                .genericModelSubstitutes(ResponseEntity.class)
                .globalResponseMessage(GET, asList(
                        new ResponseMessageBuilder()
                                .code(NO_CONTENT.value())
                                .message(NO_CONTENT.getReasonPhrase())
                                .build(),
                        new ResponseMessageBuilder()
                                .code(INTERNAL_SERVER_ERROR.value())
                                .message(INTERNAL_SERVER_ERROR.getReasonPhrase())
                                .build()))
                .apiInfo(new ApiInfoBuilder().title("Gamesys Technical Test - Time Travel API").description(
                        "The purpose of this API is to expose operations of time travels")
                        .contact(new Contact("Guilherme Iobbi (guilherme.iobbi@gmail.com)", EMPTY, EMPTY))
                        .build());
    }
}
