package com.gamesys.timetravel.api.resource;

import com.gamesys.timetravel.api.exception.BadRequestException;
import com.gamesys.timetravel.api.resource.request.TimeTravelRequest;
import com.gamesys.timetravel.api.resource.response.TimeTravelResponse;
import com.gamesys.timetravel.core.service.TimeTravelService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.Pattern;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import static com.gamesys.timetravel.api.config.APIMessages.SEARCH_AT_LEAST_ONE_FIELD_REQUIRED;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;
import static org.springframework.util.StringUtils.isEmpty;

@Slf4j
@Validated
@RestController
@RequestMapping("/time-travels")
public class TimeTravelResource {

    public static final String PGI_PATTERN = "[a-zA-Z]{1}[a-zA-Z0-9]{4,9}";

    @Autowired
    private TimeTravelService timeTravelService;

    @ApiOperation(value = "Register a new time travel")
    @PostMapping
    @ResponseStatus(CREATED)
    public TimeTravelResponse register(@ApiParam(value = "Request body", required = true)
        @RequestBody @Valid TimeTravelRequest request) {
        log.info("m=register, request={}", request);
        val response = TimeTravelResponse.fromDto(timeTravelService.registerTimeTravel(TimeTravelRequest.fromRequest(request)));
        log.info("m=register, response={}", response);
        return response;
    }

    @ApiOperation(value = "Search for of time travels")
    @GetMapping(value = "/search", produces = APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(OK)
    public List<TimeTravelResponse> searchByParameters(
        @ApiParam(value = "Personal galactic identifier") @RequestParam(required = false) @Pattern(message = "{time-travel.pgi.invalid}", regexp = PGI_PATTERN)
            final String pgi,
        @ApiParam(value = "Time travel destination") @RequestParam(required = false)
            final String place,
        @ApiParam(value = "Time travel date") @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
            final LocalDateTime date) {
        log.info("m=searchByParameters, pgi={}, place={}, date={}", pgi, place, date);
        if (isEmpty(pgi) && isEmpty(place) && isEmpty(date)) {
            throw new BadRequestException(SEARCH_AT_LEAST_ONE_FIELD_REQUIRED);
        }
        val response = timeTravelService.searchByParameters(pgi, place, date)
            .stream()
            .map(TimeTravelResponse::fromDto)
            .collect(Collectors.toList());
        log.info("m=searchByParameters, response={}", response);
        return response;
    }
}
