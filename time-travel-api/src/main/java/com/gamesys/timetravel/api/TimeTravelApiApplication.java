package com.gamesys.timetravel.api;

import com.gamesys.timetravel.api.config.TimeTravelApiConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import({TimeTravelApiConfig.class})
public class TimeTravelApiApplication {

	  public static void main(String[] args) {
		    SpringApplication.run(TimeTravelApiApplication.class, args);
	  }
}
