package com.gamesys.timetravel.api.config;

public abstract class APIMessages {

    public static final String PGI_EXISTENT = "user already time traveled to that moment and place";
    public static final String SEARCH_AT_LEAST_ONE_FIELD_REQUIRED = "search must have at least one filter";
}
