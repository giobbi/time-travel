package com.gamesys.timetravel.api.advice;

import com.gamesys.timetravel.api.dto.ResponseError;
import com.gamesys.timetravel.api.exception.BadRequestException;
import com.gamesys.timetravel.core.exception.ResourceNotFoundException;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.persistence.PersistenceException;
import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.gamesys.timetravel.api.config.APIMessages.PGI_EXISTENT;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.CONFLICT;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@Slf4j
@RestControllerAdvice
public class ControllerAdvice extends ResponseEntityExceptionHandler {

    @ResponseStatus(CONFLICT)
    @ExceptionHandler(PersistenceException.class)
    protected ResponseError handleExistentTimeTravelException(final Exception ex) {
        log.warn("m=handleExistentTimeTravelException, ex={}", ex.getLocalizedMessage());
        return ResponseError.builder()
            .errors(Arrays.asList(PGI_EXISTENT))
            .build();
    }

    @ResponseStatus(BAD_REQUEST)
    @ExceptionHandler(ConstraintViolationException.class)
    protected ResponseError handleConstraintViolationException(final ConstraintViolationException ex) {
        log.warn("m=handleConstraintViolationException, ex={}", ex.getLocalizedMessage());
        return ResponseError.builder()
            .errors(Arrays.asList(ex.getLocalizedMessage()))
            .build();
    }

    @ResponseStatus(BAD_REQUEST)
    @ExceptionHandler(BadRequestException.class)
    protected ResponseError handleBadRequestException(final BadRequestException ex) {
        log.warn("m=handleBadRequestException, ex={}", ex.getLocalizedMessage());
        return ResponseError.builder()
                .errors(Arrays.asList(ex.getLocalizedMessage()))
                .build();
    }

    @ResponseStatus(NOT_FOUND)
    @ExceptionHandler(ResourceNotFoundException.class)
    protected ResponseEntity handleResourceNotFoundException(final ResourceNotFoundException ex) {
        log.warn("m=handleResourceNotFoundException, ex={}", ex.getLocalizedMessage());
        return ResponseEntity.notFound().build();
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(final MethodArgumentNotValidException ex,
        final HttpHeaders headers, final HttpStatus status, final WebRequest request) {
        log.warn("m=handleMethodArgumentNotValid, ex={}", ex.getLocalizedMessage());
        final List<Object> errors = new ArrayList<>();
        ex.getBindingResult().getFieldErrors().forEach(err -> {
            errors.add(err.getField() + " " + err.getDefaultMessage());
        });
        val dto = ResponseError.builder()
            .errors(errors)
            .build();
        return ResponseEntity.badRequest().headers(headers).body(dto);
    }
}
