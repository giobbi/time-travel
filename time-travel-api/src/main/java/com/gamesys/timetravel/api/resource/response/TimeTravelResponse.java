package com.gamesys.timetravel.api.resource.response;

import com.gamesys.timetravel.core.dto.TimeTravelDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TimeTravelResponse {

    @ApiModelProperty(value = "Entity id", example = "1")
    private Long id;

    @ApiModelProperty(value = "Personal galactic identifier", example = "P12345")
    private String pgi;

    @ApiModelProperty(value = "Time travel destination", example = "london")
    private String place;

    @ApiModelProperty(value = "Time travel date", example = "2018-07-25T12:15:10")
    private LocalDateTime date;

    public static TimeTravelResponse fromDto(final TimeTravelDTO dto) {
        return TimeTravelResponse.builder()
            .id(dto.getId())
            .pgi(dto.getPgi())
            .date(dto.getDate())
            .place(dto.getPlace())
            .build();
    }
}
