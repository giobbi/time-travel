package com.gamesys.timetravel.api.resource.request;

import com.gamesys.timetravel.core.dto.TimeTravelDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

import static com.gamesys.timetravel.api.resource.TimeTravelResource.PGI_PATTERN;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TimeTravelRequest {

    @NotBlank
    @Pattern(message = "{time-travel.pgi.invalid}", regexp = PGI_PATTERN)
    @ApiModelProperty(value = "Personal galactic identifier", example = "P12345", required = true)
    private String pgi;

    @NotBlank
    @Size(max = 40, message = "{time-travel.place.size}")
    @ApiModelProperty(value = "Time travel destination", example = "london", required = true)
    private String place;

    @NotNull
    @ApiModelProperty(value = "Time travel date", example = "2018-07-25T12:15:10", required = true)
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime date;

    public static TimeTravelDTO fromRequest(final TimeTravelRequest request) {
        return TimeTravelDTO.builder()
            .pgi(request.getPgi())
            .place(request.getPlace())
            .date(request.getDate())
            .build();
    }

}
