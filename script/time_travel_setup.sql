use time_travel;

CREATE TABLE IF NOT EXISTS time_travel.time_travel (
    time_travel_id BIGINT NOT NULL AUTO_INCREMENT             COMMENT 'Travel time table ID',
    desc_pgi VARCHAR(10)                                      COMMENT 'Personal galactic identifier',
    desc_place VARCHAR(40)                                    COMMENT 'Destination of the time travel',
    dat_travel TIMESTAMP NOT NULL                             COMMENT 'Date of the time travel',
    dat_creation TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL COMMENT 'Creation date',

    PRIMARY KEY (time_travel_id),
    CONSTRAINT timetravel_uk01 UNIQUE (desc_pgi, desc_place, dat_travel)
)
ENGINE=INNODB
COMMENT 'Table to save time travel details';

CREATE INDEX timetravel_idx01_pgi   ON time_travel (desc_pgi);
CREATE INDEX timetravel_idx02_place ON time_travel (desc_place);
CREATE INDEX timetravel_idx03_date  ON time_travel (dat_travel);
