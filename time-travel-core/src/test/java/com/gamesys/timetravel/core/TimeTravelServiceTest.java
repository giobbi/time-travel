package com.gamesys.timetravel.core;

import com.gamesys.timetravel.core.dto.TimeTravelDTO;
import com.gamesys.timetravel.core.entity.TimeTravelEntity;
import com.gamesys.timetravel.core.exception.ResourceNotFoundException;
import com.gamesys.timetravel.core.repository.TimeTravelRepository;
import com.gamesys.timetravel.core.service.TimeTravelService;
import lombok.val;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.data.domain.Example;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class TimeTravelServiceTest {

    @InjectMocks
    private TimeTravelService timeTravelService;

    @Mock
    private TimeTravelRepository timeTravelRepository;

    private static final String PGI = "P12345";
    private static final String PLACE = "london";
    private static final LocalDateTime DATE = LocalDateTime.now();

    @Test
    public void registerTimeTravelTest() {
        // arrange
        val dto = TimeTravelDTO.builder()
            .pgi(PGI)
            .place(PLACE)
            .date(DATE)
            .build();
        when(timeTravelRepository.save(any())).thenReturn(new TimeTravelEntity());

        // act
        timeTravelService.registerTimeTravel(dto);

        // assert
        verify(timeTravelRepository, atLeastOnce()).save((any()));
    }

    @Test
    public void searchByParametersTest_withResults() {
        // arrange
        val entity_1 = TimeTravelEntity.builder()
            .pgi(PGI)
            .place(PLACE)
            .date(DATE)
            .build();
        val entities = Arrays.asList(entity_1);
        when(timeTravelRepository.findAll(Mockito.any(Example.class)))
            .thenReturn(entities);

        // act
        val dtoList = timeTravelService.searchByParameters(PGI, PLACE, DATE);

        // assert
        assertEquals(1, dtoList.size());
        dtoList.forEach(dto -> {
            assertEquals(entity_1.getPgi(), dto.getPgi());
            assertEquals(entity_1.getPlace(), dto.getPlace());
            assertEquals(entity_1.getDate(), dto.getDate());
        });
    }

    @Test(expected = ResourceNotFoundException.class)
    public void searchByParametersTest_withoutResults() {
        // arrange
        when(timeTravelRepository.findAll(any(Example.class)))
            .thenReturn(Collections.emptyList());

        // act
        val dtoList = timeTravelService.searchByParameters(PGI, null, null);
    }
}
