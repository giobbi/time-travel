package com.gamesys.timetravel.core.service;

import com.gamesys.timetravel.core.dto.TimeTravelDTO;
import com.gamesys.timetravel.core.entity.TimeTravelEntity;
import com.gamesys.timetravel.core.exception.ResourceNotFoundException;
import com.gamesys.timetravel.core.repository.TimeTravelRepository;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class TimeTravelService {

    @Autowired
    private TimeTravelRepository timeTravelRepository;

    public TimeTravelDTO registerTimeTravel(final TimeTravelDTO dto) {
        val entity = TimeTravelEntity.builder()
            .date(dto.getDate())
            .place(dto.getPlace())
            .pgi(dto.getPgi())
            .build();
        return TimeTravelDTO.fromEntity(timeTravelRepository.save(entity));
    }

    public List<TimeTravelDTO> searchByParameters(final String pgi, final String place,
        final LocalDateTime date) {
        val entity = TimeTravelEntity.builder()
            .pgi(pgi)
            .place(place)
            .date(date)
            .build();
        val results = Lists.newArrayList(timeTravelRepository.findAll(Example.of(entity)));
        if (results.isEmpty()) {
            throw new ResourceNotFoundException();
        }
        val dtoList = new ArrayList<TimeTravelDTO>();
        results.forEach(e -> dtoList.add(TimeTravelDTO.fromEntity(e)));
        return dtoList;
    }

}
