package com.gamesys.timetravel.core;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackageClasses = {CoreConfig.class})
@EnableAutoConfiguration
public class CoreConfig {
}
