package com.gamesys.timetravel.core.repository;

import com.gamesys.timetravel.core.entity.TimeTravelEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TimeTravelRepository extends JpaRepository<TimeTravelEntity, Long> {
}
