package com.gamesys.timetravel.core.dto;

import com.gamesys.timetravel.core.entity.TimeTravelEntity;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class TimeTravelDTO {

    private Long id;
    private String pgi;
    private String place;
    private LocalDateTime date;

    public static TimeTravelDTO fromEntity(final TimeTravelEntity entity) {
        return TimeTravelDTO.builder()
                .id(entity.getId())
                .pgi(entity.getPgi())
                .date(entity.getDate())
                .place(entity.getPlace())
                .build();
    }

}
