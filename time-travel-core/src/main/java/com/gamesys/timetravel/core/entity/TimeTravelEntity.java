package com.gamesys.timetravel.core.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "TimeTravel")
@Table(name = "time_travel")
public class TimeTravelEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "time_travel_id", nullable = false, updatable = false)
    private Long id;

    @Column(name = "desc_pgi", nullable = false, updatable = false)
    private String pgi;

    @Column(name = "desc_place", nullable = false, updatable = false)
    private String place;

    @Column(name = "dat_travel", nullable = false, updatable = false)
    private LocalDateTime date;

    @Column(name = "dat_creation", insertable = false, nullable = false, updatable = false)
    private LocalDateTime creationDate;
}
